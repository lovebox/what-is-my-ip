# what-is-my-ip

get your current ip address on the local-network (fork of [https://github.com/dominictarr/my-local-ip](https://github.com/dominictarr/my-local-ip)) 

In case of multiple IPs (typically, wifi and ethernet), you are prompted to select the one to choose.


## In case of only one possible IP
```
console.log(require('what-is-my-ip')())
//==> 192.168.1.33
```

## In case of multiple IPs

```
Please choose which IP to use:
0:192.168.1.1
1:128.31.34.224
Which one do you chose?1
128.31.34.224
```

Also, because sometime you need this (cross-platform!)
```
> npm install -g what-is-my-ip
> what-is-my-ip
128.31.34.224
```

## License

MIT