#! /usr/bin/env node

var n = require('os').networkInterfaces();
var prompt = require('prompt-sync')();

var myIp = module.exports = function () {
  var ips = [];
  for (var k in n) {
    var inter = n[k];
    for (var j in inter) {
      if (inter[j].family === 'IPv4' && !inter[j].internal) {
        ips.push(inter[j].address);
      }
    }
  }
  if (ips.length == 0) {
    console.error("No local IP found.");
    return;
  } else if (ips.length == 1) {
    return ips[0];
  } else {
    console.log("Please choose which IP to use:");
    for (var k = 0; k < ips.length; k++) {
      console.log(k + ":" + ips[k]);
    }
    var k = parseInt(prompt('Which one do you chose?'));
    if (k < 0 || k >= ips.length) {
      console.error("Chosen IP out of range");
      return;
    }
    return ips[k];
  }
}

if (!module.parent)
  return console.log(myIp())
